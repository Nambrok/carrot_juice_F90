PROGRAM carrot_juice
!creation of carrots
  use growing
  use pressing

  IMPLICIT NONE

  integer, dimension(1000) :: field, carrots
  real :: juice

  
  call plant(field, 1000, 1)
  call grow(field, 1000, 3)
  call harvest(field, carrots, 1000)
  call press(carrots, 1000, 3.14, juice)

  print*, juice, " carrot juices successfully created"

END PROGRAM !C'est Damien
!C'est pas vrai
