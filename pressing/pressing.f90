MODULE pressing
  IMPLICIT NONE
CONTAINS
  !This program press carrots for others

  SUBROUTINE press(carrots, size, pressing_factor, juice)
    INTEGER, INTENT(in) :: size
    INTEGER, DIMENSION(size), INTENT(inout) :: carrots
    REAL, INTENT(in) :: pressing_factor
    REAL, INTENT(out) :: juice
    INTEGER :: i

    juice = 0.0
    DO i=1, size
       juice = juice + carrots(i)/pressing_factor
    END DO
  END SUBROUTINE press
END MODULE pressing
